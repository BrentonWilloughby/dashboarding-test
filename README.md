# Dashboarding Test

This project is to test the method of creating similar dashboards in various data visualization platforms.  
Primary goals are to a) learn the basics of how to interact with data in a solution,  2) determine what solutions are most intuitive for me, and c) determine what solutions would be 'best' for people I work with. 

The data set is the results of the 2018 Olympics and a Fantasy Olympics draft I coerced work teammates to participate in.  
I hope I win...